## Privacy policy for Watch Ouch!

#### General

Watch Ouch! is a game app without any intentions to store user data unless strictly necessary. The data stored inside the app (config etc) will never leave your device. The data stored in Firebase (see below) is completely anonymous and can never be used to trace or identify a real person. Should you decide to remove the app, all ties between you and us will be cut, even if you reinstall the app at a later time. You would become a completely new user!

#### Firebase

Watch Ouch! uses the analytics tool Firebase to detect any errors and crashes that may occur in the app. Firebase also gives us basic information about how the app is used. This information is important for us in order to improve and further develop Watch Ouch! efficiently. You can read more about Firebase [here](https://firebase.google.com/).

#### The future

As Watch Ouch! grows and gets better, there may come a time when we need to save data that we have not needed to save before. However, we will never make that kind of change without also notifying you, and the latest information is always available here. Furthermore, this page is version managed to allow you to follow just about any changes made to the privacy policy.